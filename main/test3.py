import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


from src.awaken2.baseic.environment.common import environment_check_dependent_files_exists
environment_check_dependent_files_exists()
