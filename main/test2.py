import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import asyncio
from src.awaken2.server.ws.awaken_ws import main

asyncio.run(main())
