import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import json
from src.awaken2.core.perform.perform_pool import PerFormPool
from src.awaken2.server.db import DB
from src.awaken2.server.db import SQL
from src.awaken2.baseic.keyword import G_KEYWORD
from src.awaken2.core.interpreter.grammar_parser import GRAMMAR_PARSER
from src.awaken2.kit.engine_agency import EngineAgency


# 从数据库中获取数据
db_data = DB.execute(SQL.Tasks.get_task_script(1))
task_info = db_data.get(G_KEYWORD.Common.TransactionFieldKey.Items)[0]
task_type = task_info.get(G_KEYWORD.DataBase.TasksFieldKey.Type)
task_name = task_info.get(G_KEYWORD.DataBase.TasksFieldKey.Name)
task_docs = task_info.get(G_KEYWORD.DataBase.TasksFieldKey.Docs)
script_json = json.loads(task_info.get(G_KEYWORD.DataBase.TasksFieldKey.ScriptJson))

# 解析成 Awaken 语法
awaken_script_code = []
awaken_script_code.append(f'TaskType = \'{task_type}\'')
awaken_script_code.append(f'TaskName = \'{task_name}\'')
awaken_script_code.append(f'TaskDocs = \'{task_docs}\'')

if len(script_json['cases']) > 0:
    for case in script_json['cases']:
        # 声明用例
        case_statement = f'CASE :: {case["name"]}'
        if case['docs'] != '':
            case_statement += f' :: {case["docs"]}'
        awaken_script_code.append(case_statement)

        if len(case['logic']) > 0:
            for logic in case['logic']:
                # TODO 暂未处理不同的逻辑块类型
                driving_character = logic['list'][0]['drivingCharacter']
                driving_parameter = []
                for para_value in logic['list'][0]['drivingParameter']:
                    if para_value['value'] != '':
                        driving_parameter.append(para_value['value'])
                
                run_instructions = '    '
                if logic['list'][0]['whetherThereReturn']:
                    if logic['list'][0]['namespaceKey'] != '':
                        if logic['list'][0]['namespaceScope'] == 'UNIVERSE':
                            run_instructions += '$'
                        run_instructions += f"{logic['list'][0]['namespaceKey']} = "
                    
                run_instructions += f'{ driving_character }'
                for para in driving_parameter:
                    run_instructions += f'  >>  {para}'

                awaken_script_code.append(run_instructions)
        awaken_script_code.append('    ?? True')

task_object = GRAMMAR_PARSER.parsing_json(awaken_script_code, task_type, 1)

# p = PerFormPool()
# p.put_task(task_object)
# p.running()

e = EngineAgency()
e.start(task_object)
