"""
[ unit testing :: awaken.core.interpreter.base_code_converter.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))


from pathlib import Path

from src.awaken2.baseic.const import G_CONST
from src.awaken2.core.interpreter.base_code_converter import BaseCodeConverter
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception


# --------------------------------------------------------------------------
class TestCoreInterpreterBaseCodeConverter(object):
    """
    [ unit testing :: awaken.core.interpreter.base_code_converter.py ]

    """
    base_code_converter: BaseCodeConverter
    test_file_path: Path


    def setup_class(self):
        self.test_file_path = Path('./EngineeringData/test.awaken-web')


    def setup_method(self):
        self.base_code_converter = BaseCodeConverter()


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_universe_give(self):
        """
        [ 公域赋值测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            Para {G_CONST.Interpreter.GrammarSymbol.Give} Value
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.Give} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Para Value'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_02_universe_give(self):
        """
        [ 公域赋值测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.GrammarSymbol.Quote}Para {G_CONST.Interpreter.GrammarSymbol.Give} Value
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.Give} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Para Value'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_01_privately_give(self):
        """
        [ 私域赋值测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                CasePara {G_CONST.Interpreter.GrammarSymbol.Give} Value
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.Give} {G_CONST.Interpreter.CodeLineScopet.Local}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}CasePara Value'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_02_privately_give(self):
        """
        [ 私域赋值测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Quote}CasePara {G_CONST.Interpreter.GrammarSymbol.Give} Value
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.Give} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}CasePara Value'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_01_universe_rgive(self):
        """
        [ 公域执行并赋值测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            Para {G_CONST.Interpreter.GrammarSymbol.Give} Logout {G_CONST.Interpreter.GrammarSymbol.Run} Text
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.RGive} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Para {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout Text'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_02_universe_rgive(self):
        """
        [ 公域执行并赋值测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            Para {G_CONST.Interpreter.GrammarSymbol.Give} {G_CONST.Interpreter.GrammarSymbol.Call}Logout
            """
        )
        try:
            self.base_code_converter.convert_file(self.test_file_path)
            assert False
        except:
            assert True


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_03_universe_rgive(self):
        """
        [ 公域执行并赋值测试 3 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.GrammarSymbol.Quote}Para {G_CONST.Interpreter.GrammarSymbol.Give} {G_CONST.Interpreter.GrammarSymbol.Call}Logout
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.RGive} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Para {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_01_privately_rgive(self):
        """
        [ 私域执行并赋值测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                CasePara {G_CONST.Interpreter.GrammarSymbol.Give} Logout
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.RGive} {G_CONST.Interpreter.CodeLineScopet.Local}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}CasePara {G_CONST.Interpreter.CodeLineScopet.Local}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_02_privately_rgive(self):
        """
        [ 私域执行并赋值测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                CasePara {G_CONST.Interpreter.GrammarSymbol.Give} {G_CONST.Interpreter.GrammarSymbol.Call}Logout
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.RGive} {G_CONST.Interpreter.CodeLineScopet.Local}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}CasePara {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_03_privately_rgive(self):
        """
        [ 私域执行并赋值测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Quote}CasePara {G_CONST.Interpreter.GrammarSymbol.Give} {G_CONST.Interpreter.GrammarSymbol.Call}Logout
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.RGive} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}CasePara {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_01_statement(self):
        """
        [ 声明用例测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.SCase} TESTCASE'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_02_statement(self):
        """
        [ 声明用例测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE {G_CONST.Interpreter.GrammarSymbol.Statement} DOC
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.SCase} TESTCASE DOC'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_03_statement(self):
        """
        [ 声明用例测试 3 :: 声明时不携带参数 :: 失败 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement}
            """
        )
        try:
            self.base_code_converter.convert_file(self.test_file_path)
            assert False
        except BaseException:
            assert True


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_06_01_universe_run(self):
        """
        [ 公域执行测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            YAML
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}YAML'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_06_02_universe_run(self):
        """
        [ 公域执行测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.GrammarSymbol.Call}YAML
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}YAML'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_06_03_universe_run(self):
        """
        [ 公域执行测试 3 ]

        """
        self.test_file_path.write_text(
            f"""
            YAML {G_CONST.Interpreter.GrammarSymbol.Run} P1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}YAML P1'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_06_04_universe_run(self):
        """
        [ 公域执行测试 4 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.GrammarSymbol.Call}YAML {G_CONST.Interpreter.GrammarSymbol.Run} P1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[0] == f'2 {G_CONST.Interpreter.KEYWORD_IDENT_SCOPE_UNIVERSE} {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}YAML P1'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_07_01_privately_rgive(self):
        """
        [ 私域执行测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                Logout
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Local}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_07_02_privately_rgive(self):
        """
        [ 私域执行测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                Logout {G_CONST.Interpreter.GrammarSymbol.Run} 'debug'
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f"3 TESTCASE {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Local}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout 'debug'"


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_07_03_privately_rgive(self):
        """
        [ 私域执行测试 3 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Call}Logout
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_07_04_privately_rgive(self):
        """
        [ 私域执行测试 4 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Call}Logout {G_CONST.Interpreter.GrammarSymbol.Run} 'debug'
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f"3 TESTCASE {G_CONST.Interpreter.CodeLineType.Run} {G_CONST.Interpreter.CodeLineScopet.Global}{G_CONST.Interpreter.GrammarSymbol.ScopePrefix}Logout 'debug'"


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_08_01_case_assert(self):
        """
        [ 用例断言测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert} True
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.Assert} True'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_08_02_case_assert(self):
        """
        [ 用例断言测试 2 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1 == 1
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'3 TESTCASE {G_CONST.Interpreter.CodeLineType.Assert} 1 == 1'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_08_03_case_assert(self):
        """
        [ 用例断言测试 3 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert}
            """
        )
        try:
            self.base_code_converter.convert_file(self.test_file_path)
            assert False
        except BaseException:
            assert True


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_08_04_case_assert(self):
        """
        [ 用例断言测试 4 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1 ==
            """
        )
        try:
            self.base_code_converter.convert_file(self.test_file_path)
            assert False
        except BaseException:
            assert True


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_09_01_decorator(self):
        """
        [ 用例装饰器声明测试 1 ]

        """
        self.test_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Deco} {G_CONST.Interpreter.GrammarSymbol.Statement} DECO1KEY {G_CONST.Interpreter.GrammarSymbol.Statement} DECO1VALUE
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert} True
            """
        )
        _, basecodelines = self.base_code_converter.convert_file(self.test_file_path)
        assert basecodelines[1] == f'2 TESTCASE {G_CONST.Interpreter.CodeLineType.SDecorator} DECO1KEY DECO1VALUE'
        