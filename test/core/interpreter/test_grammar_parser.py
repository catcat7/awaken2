"""
[ unit testing :: awaken.core.interpreter.test_grammar_parser.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))

from pathlib import Path

from src.awaken2.baseic.const import G_CONST
from src.awaken2.baseic.structural.interpreter import AwakenTask
from src.awaken2.core.interpreter.grammar_parser import GrammarParser
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception


class TestCoreInterpreterGrammarParser(object):
    """
    [ unit testing :: awaken.core.interpreter.grammar_parser.py ]

    """
    grammar_parser: GrammarParser
    test_web_file_path: Path


    def setup_class(self):
        self.test_web_file_path = Path('./EngineeringData/test.awaken-web')
        self.test_api_file_path = Path('./EngineeringData/test.awaken-api')


    def setup_method(self):
        self.grammar_parser = GrammarParser()


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_parsing(self):
        """
        [ 脚本解释器解析文件 1 ]

        """
        self.test_web_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        task = self.grammar_parser.parsing_file(self.test_web_file_path)
        assert type(task) == AwakenTask and task.task_type == G_CONST.Type.Task.Web


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_02_parsing(self):
        """
        [ 脚本解释器解析文件 2 ]

        """
        self.test_api_file_path.write_text(
            f"""
            {G_CONST.Interpreter.StatementIdent.Case} {G_CONST.Interpreter.GrammarSymbol.Statement} TESTCASE
                {G_CONST.Interpreter.GrammarSymbol.Assert} 1
            """
        )
        task = self.grammar_parser.parsing_file(self.test_api_file_path)
        assert type(task) == AwakenTask and task.task_type == G_CONST.Type.Task.Api
