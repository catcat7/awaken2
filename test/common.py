

def capture_exception_decorator(cls):
    
    def _capture_exception_decorator(*args, **kwargs):
        try:
            cls(*args, **kwargs)
            assert True
        except BaseException:
            assert False
    
    return _capture_exception_decorator
