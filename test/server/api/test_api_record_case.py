"""
[ unit testing :: awaken.server.api.blueprint.api_record_case.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))


import requests

from src.awaken2.core.perform.pool_runtime import POOL_RUNTIME
from src.awaken2.baseic.keyword import G_KEYWORD
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception
from src.awaken2.server.db import DB
from src.awaken2.server.db import SQL
from src.awaken2.server.db.sqlite_statement_handle import G_SQLITE_STATEMENT_HANDLE
from src.awaken2.server.api.awaken_app import AwakenApp
from src.awaken2.server.api.server_request_handler import URL_PREFIX
from src.awaken2.server.api.blueprint.api_record_case import POST_RECORD_CASE_GET_COUNT
from src.awaken2.server.api.blueprint.api_record_case import POST_RECORD_CASE_GET_LIST
from src.awaken2.server.api.blueprint.api_record_case import POST_RECORD_CASE_GET_SPEND_TIME


# --------------------------------------------------------------------------
class TestApiRecordCase:
    """
    [ unit testing :: awaken.server.api.blueprint.api_record_case.py ]

    """
    port: int
    host: str
    web_api_server: AwakenApp


    # ----------------------------------------------------------------------
    def setup_class(self):
        self.port = 7777
        self.host = f'http://127.0.0.1:{self.port}{URL_PREFIX}'
        def _runing_web_api():
            self.web_api_server = AwakenApp()
            self.web_api_server.living.run(port=self.port)
        POOL_RUNTIME.pool.apply_async(func=_runing_web_api)

        # 数据库创建 10 条测试数据
        for i in range(1, 11):
            p_type = 'web' if i <= 6 else 'api'
            p_date = '2022-02-02' if i <= 9 else '2022-11-11'
            p_state = 'state1' if i <= 3 else 'state2'
            DB.execute(SQL.CaseRecord.creation(
                trid=1,
                name=f'name{i}',
                docs=f'docs{i}',
                type=p_type,
                created_date=p_date,
                start_time=f'start_time{i}',
                state=p_state,
            ))


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_get_count(self):
        """
        [ 获取用例记录数量 1 ]

        """
        url = f'{self.host}{POST_RECORD_CASE_GET_COUNT}'
        r = requests.post(
            url=url
        ).json()
        code = r.get(G_KEYWORD.Api.TransactionFieldKey.Code)
        assert code == 0


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_02_get_count(self):
        """
        [ 获取用例记录数量 2 ]

        """
        url = f'{self.host}{POST_RECORD_CASE_GET_COUNT}'
        r = requests.post(url=url).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 10


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_03_get_count(self):
        """
        [ 获取用例记录数量 3 ]

        """
        url = f'{self.host}{POST_RECORD_CASE_GET_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.CaseRecordFieldKey.CRid: 1}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 1


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_04_get_count(self):
        """
        [ 获取用例记录数量 4 ]

        """
        url = f'{self.host}{POST_RECORD_CASE_GET_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.CaseRecordFieldKey.Type: 'web'}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 6


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_05_get_count(self):
        """
        [ 获取用例记录数量 5 ]

        """
        url = f'{self.host}{POST_RECORD_CASE_GET_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.CaseRecordFieldKey.CreatedDate: '2022-11-11'}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 1


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_06_get_count(self):
        """
        [ 获取用例记录数量 6 ]

        """
        url = f'{self.host}{POST_RECORD_CASE_GET_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.CaseRecordFieldKey.State: 'state2'}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 7


    # # ----------------------------------------------------------------------
    # @deco_test_capture_exception
    # def test_02_01_get_list(self):
    #     """
    #     [ 查询用例记录列表 1 ]

    #     """
    #     url = f'{self.host}{POST_RECORD_CASE_GET_LIST}'
    #     r = requests.post(
    #         url=url,
    #         data={
    #             G_KEYWORD.Api.CaseRecordFieldKey.Type: 'api',
    #             G_KEYWORD.Api.CaseRecordFieldKey.State: 'state2',
    #         }
    #     ).json()
    #     totai = r.\
    #         get(G_KEYWORD.Api.TransactionFieldKey.Result).\
    #         get(G_KEYWORD.Api.TransactionFieldKey.Totai)
    #     assert totai == 4
 