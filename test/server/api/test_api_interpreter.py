"""
[ unit testing :: awaken.server.api.blueprint.api_interpreter.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))


import requests

from src.awaken2.kit.web.web_engine_method_map import WEB_INSTRUCTIONS
from src.awaken2.core.perform.pool_runtime import POOL_RUNTIME
from src.awaken2.baseic.keyword import G_KEYWORD
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception
from src.awaken2.server.api.awaken_app import AwakenApp
from src.awaken2.server.api.server_request_handler import URL_PREFIX
from src.awaken2.server.api.blueprint.api_interpreter import POST_INTERPRETER_GET_INSTRUCTIONS


# --------------------------------------------------------------------------
class TestApiInterpreter:
    """
    [ unit testing :: awaken.server.api.blueprint.api_interpreter.py ]

    """
    port: int
    host: str
    web_api_server: AwakenApp


    # ----------------------------------------------------------------------
    def setup_class(self):
        self.port = 7777
        self.host = f'http://127.0.0.1:{self.port}{URL_PREFIX}'
        def _runing_web_api():
            self.web_api_server = AwakenApp()
            self.web_api_server.living.run(port=self.port)
        POOL_RUNTIME.pool.apply_async(func=_runing_web_api)


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_post_instructions(self):
        """
        [ 获取WEB指令集 ]

        """
        url = f'{self.host}{POST_INTERPRETER_GET_INSTRUCTIONS}'
        r = requests.post(
            url=url,
            data={'task_type': 'web'}
        ).json()
        instructions  = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.InterpreterFieldKey.Instructions)
        assert instructions == WEB_INSTRUCTIONS
