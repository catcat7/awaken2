"""
[ unit testing :: awaken.server.api.blueprint.api_project.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))


import requests

from src.awaken2.core.perform.pool_runtime import POOL_RUNTIME
from src.awaken2.baseic.keyword import G_KEYWORD
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception
from src.awaken2.server.db import DB
from src.awaken2.server.db import SQL
from src.awaken2.server.db.sqlite_statement_handle import G_SQLITE_STATEMENT_HANDLE
from src.awaken2.server.api.awaken_app import AwakenApp
from src.awaken2.server.api.server_request_handler import URL_PREFIX
from src.awaken2.server.api.blueprint.api_project import POST_PROJECT_GET_PROJECT_COUNT
from src.awaken2.server.api.blueprint.api_project import POST_PROJECT_GET_PROJECT_LIST
from src.awaken2.server.api.blueprint.api_project import POST_PROJECT_CREATION
from src.awaken2.server.api.blueprint.api_project import POST_PROJECT_MODIFY
from src.awaken2.server.api.blueprint.api_project import POST_PROJECT_DELETE


# --------------------------------------------------------------------------
class TestApiProject:
    """
    [ unit testing :: awaken.server.api.blueprint.api_project.py ]

    """
    port: int
    host: str
    web_api_server: AwakenApp


    # ----------------------------------------------------------------------
    def setup_class(self):
        self.port = 7777
        self.host = f'http://127.0.0.1:{self.port}{URL_PREFIX}'
        def _runing_web_api():
            self.web_api_server = AwakenApp()
            self.web_api_server.living.run(port=self.port)
        POOL_RUNTIME.pool.apply_async(func=_runing_web_api)

        # 数据库创建 10 条测试数据
        for i in range(1, 11):
            p_type = 'web' if i <= 6 else 'api'
            p_date = '2022-02-02' if i <= 9 else '2022-11-11'
            p_state = 'state1' if i <= 3 else 'state2'
            DB.execute(SQL.Project.creation(
                type=p_type,
                name=f'name{i}',
                docs=f'docs{i}',
                state=p_state,
                created_date=p_date
            ))


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_get_count(self):
        """
        [ 查询项目数量 1 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_COUNT}'
        r = requests.post(
            url=url
        ).json()
        code = r.get(G_KEYWORD.Api.TransactionFieldKey.Code)
        assert code == 0


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_02_get_count(self):
        """
        [ 查询项目数量 2 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_COUNT}'
        r = requests.post(url=url).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 10


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_03_get_count(self):
        """
        [ 查询项目数量 3 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.ProjectFieldKey.Pid: 1}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 1


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_04_get_count(self):
        """
        [ 查询项目数量 4 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.ProjectFieldKey.Type: 'web'}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 6


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_05_get_count(self):
        """
        [ 查询项目数量 5 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.ProjectFieldKey.CreatedDate: '2022-11-11'}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 1


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_06_get_count(self):
        """
        [ 查询项目数量 6 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_COUNT}'
        r = requests.post(
            url=url,
            data={G_KEYWORD.Api.ProjectFieldKey.State: 'state2'}
        ).json()
        count = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Count)
        assert count == 7


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_01_get_list(self):
        """
        [ 查询项目列表 1 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_LIST}'
        r = requests.post(
            url=url,
            data={
                G_KEYWORD.Api.ProjectFieldKey.Type: 'api',
                G_KEYWORD.Api.ProjectFieldKey.State: 'state2',
            }
        ).json()
        totai = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Totai)
        assert totai == 4


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_02_get_list(self):
        """
        [ 查询项目列表 2 ]

        """
        url = f'{self.host}{POST_PROJECT_GET_PROJECT_LIST}'
        r = requests.post(
            url=url,
            data={
                G_KEYWORD.Common.PagingDataFieldKey.Page: 1,
                G_KEYWORD.Common.PagingDataFieldKey.Number: 1,
            }
        ).json()
        structure = G_SQLITE_STATEMENT_HANDLE._table_structure.get(G_KEYWORD.DataBase.TableName.Project).keys()
        item = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Items)
        assert item[0].keys() == structure


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_01_post_creation(self):
        """
        [ 项目创建 1 ]

        """
        url1 = f'{self.host}{POST_PROJECT_CREATION}'
        url2 = f'{self.host}{POST_PROJECT_GET_PROJECT_LIST}'
        r = requests.post(
            url=url1,
            data={
                G_KEYWORD.Api.ProjectFieldKey.Type: 'web',
                G_KEYWORD.Api.ProjectFieldKey.Name: 'name',
                G_KEYWORD.Api.ProjectFieldKey.Docs: 'docs',
            }
        ).json()
        project_id = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.ProjectFieldKey.Pid)
        r = requests.post(
            url=url2,
            data={
                G_KEYWORD.Api.ProjectFieldKey.Pid: project_id
            }
        ).json()
        project_totai = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Totai)
        assert project_totai == 1


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_01_post_modify(self):
        """
        [ 项目编辑 1 ]

        """
        url1 = f'{self.host}{POST_PROJECT_MODIFY}'
        url2 = f'{self.host}{POST_PROJECT_GET_PROJECT_LIST}'
        requests.post(
            url=url1,
            data={
                G_KEYWORD.Api.ProjectFieldKey.Pid: 1,
                G_KEYWORD.Api.ProjectFieldKey.Name: 'name111',
                G_KEYWORD.Api.ProjectFieldKey.Docs: 'docs111',
            }
        ).json()
        r = requests.post(
            url=url2,
            data={
                G_KEYWORD.Api.ProjectFieldKey.Pid: 1
            }
        ).json()
        project_items = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Items)
        project_name = project_items[0].get(G_KEYWORD.Api.ProjectFieldKey.Name)
        project_docs = project_items[0].get(G_KEYWORD.Api.ProjectFieldKey.Docs)
        assert project_name == 'name111' and project_docs == 'docs111'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_01_post_modify(self):
        """
        [ 项目删除 1 ]

        """
        url1 = f'{self.host}{POST_PROJECT_DELETE}'
        url2 = f'{self.host}{POST_PROJECT_GET_PROJECT_LIST}'
        requests.post(
            url=url1,
            data={
                G_KEYWORD.Api.ProjectFieldKey.Pid: 1
            }
        ).json()
        r = requests.post(
            url=url2,
            data={
                G_KEYWORD.Api.ProjectFieldKey.Pid: 1
            }
        ).json()
        project_totai = r.\
            get(G_KEYWORD.Api.TransactionFieldKey.Result).\
            get(G_KEYWORD.Api.TransactionFieldKey.Totai)
        assert project_totai == 0
