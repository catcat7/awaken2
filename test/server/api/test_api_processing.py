"""
[ unit testing :: awaken.server.api.blueprint.api_processing.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))


import requests

from src.awaken2.core.perform.pool_runtime import POOL_RUNTIME
from src.awaken2.baseic.keyword import G_KEYWORD
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception
from src.awaken2.server.api.awaken_app import AwakenApp
from src.awaken2.server.api.server_request_handler import URL_PREFIX
from src.awaken2.server.api.blueprint.api_processing import POST_PROCESSING_GET_URL_DOM


# --------------------------------------------------------------------------
class TestApiProcessing:
    """
    [ unit testing :: awaken.server.api.blueprint.api_processing.py ]

    """
    port: int
    host: str
    web_api_server: AwakenApp


    # ----------------------------------------------------------------------
    def setup_class(self):
        self.port = 7777
        self.host = f'http://127.0.0.1:{self.port}{URL_PREFIX}'
        def _runing_web_api():
            self.web_api_server = AwakenApp()
            self.web_api_server.living.run(port=self.port)
        POOL_RUNTIME.pool.apply_async(func=_runing_web_api)


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_post_get_url_dom(self):
        """
        [ 获取目标URL的DOM文档 ]

        """
        url = f'{self.host}{POST_PROCESSING_GET_URL_DOM}'
        r = requests.post(
            url=url,
            data={'url': 'http://www.baidu.com/'}
        ).json()
        code = r.get(G_KEYWORD.Api.TransactionFieldKey.Code)
        assert code == 0
