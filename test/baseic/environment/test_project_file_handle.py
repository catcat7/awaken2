"""
[ unit testing :: awaken2.baseic.environment.project_file_handle.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))

import shutil

from src.awaken2.baseic.const import G_CONST
from src.awaken2.baseic.environment.project_file_handle import ProjectFileHandle
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception


class TestProjectFileHandle(object):
    project_file_handle: ProjectFileHandle


    def setup_class(self):
        self.project_file_handle = ProjectFileHandle()
        G_CONST.Path.FilePath.ProjectInit.touch()


    def teardown_class(self):
        G_CONST.Path.FilePath.ProjectInit.unlink()


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_create_project(self):
        """ 创建项目 """
        project_name   = 'Project1'
        create_results = self.project_file_handle.project_create(project_name)
        shutil.rmtree(G_CONST.Path.ACTUALLY_CWD.joinpath(project_name))
        assert project_name in create_results.keys()
        assert create_results[project_name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_02_create_project(self):
        """ 批量创建项目 """
        project_names  = ['Project1', 'Project2']
        create_results = self.project_file_handle.project_create(project_names)
        assert len(create_results) == 2
        for name in project_names:
            shutil.rmtree(G_CONST.Path.ACTUALLY_CWD.joinpath(name))
            assert name in create_results.keys()
            assert create_results[name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_01_delete_project(self):
        """ 删除项目 """
        project_name   = 'Project1'
        self.project_file_handle.project_create(project_name)
        delete_results = self.project_file_handle.project_delete(project_name)
        assert project_name in delete_results
        assert delete_results[project_name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_02_delete_project(self):
        """ 批量删除项目 """
        project_names  = ['Project1', 'Project2']
        self.project_file_handle.project_create(project_names)
        delete_results = self.project_file_handle.project_delete(project_names)
        assert len(delete_results) == 2
        for name in project_names:
            assert name in delete_results
            assert delete_results[name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_01_get_projects(self):
        """ 获取工程的项目列表 """
        project_names  = ['Project1', 'Project2']
        self.project_file_handle.project_create(project_names)
        project_list   = self.project_file_handle.get_projects()
        assert len(project_list) == 2
        for name in project_names:
            self.project_file_handle.project_delete(name)
            assert name in project_list


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_01_project_rename(self):
        """ 重命名工程的项目 """
        old_project_name = 'Project1'
        self.project_file_handle.project_create(old_project_name)
        new_project_name = 'Project2'
        self.project_file_handle.project_rename(old_project_name, new_project_name)
        project_list = self.project_file_handle.get_projects()
        self.project_file_handle.project_delete(new_project_name)
        assert len(project_list) == 1
        assert new_project_name in project_list
        assert old_project_name not in project_list


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_02_project_rename(self):
        """ 批量重命名工程的项目 """
        old_project_name = ['Project1', 'Project2', 'Project3']
        self.project_file_handle.project_create(old_project_name)
        new_project_name = ['Project4', 'Project5', 'Project6']
        self.project_file_handle.project_rename(old_project_name, new_project_name)
        project_list = self.project_file_handle.get_projects()
        self.project_file_handle.project_delete(new_project_name)
        assert len(project_list) == 3
        number = 0
        for name in project_list:
            assert new_project_name[number] == name
            assert old_project_name[number] != name
            number += 1


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_01_create_task(self):
        """ 创建任务 """
        project_name   = 'Project1'
        task_name      = 'Task1'
        self.project_file_handle.project_create(project_name)
        create_results = self.project_file_handle.task_create(project_name, 'web', task_name)
        shutil.rmtree(G_CONST.Path.ACTUALLY_CWD.joinpath(project_name))
        assert len(create_results) == 1
        assert task_name in create_results.keys()
        assert create_results[task_name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_02_create_task(self):
        """ 批量创建任务 """
        project_name   = 'Project1'
        task_name      = ['Task1', 'Task2', 'Task3']
        self.project_file_handle.project_create(project_name)
        create_results = self.project_file_handle.task_create(project_name, 'web', task_name)
        shutil.rmtree(G_CONST.Path.ACTUALLY_CWD.joinpath(project_name))
        assert len(create_results) == 3
        for name in task_name:
            assert name in create_results.keys()
            assert create_results[name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_06_01_delete_task(self):
        """ 删除任务 """
        project_name   = 'Project1'
        task_name      = 'Task1'
        self.project_file_handle.project_create(project_name)
        self.project_file_handle.task_create(project_name, 'web', task_name)
        delete_results = self.project_file_handle.task_delete(project_name, task_name)
        self.project_file_handle.project_delete(project_name)
        assert len(delete_results) == 1
        assert task_name in delete_results.keys()
        assert delete_results[task_name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_06_02_delete_task(self):
        """ 批量删除任务 """
        project_name   = 'Project1'
        task_name      = ['Task1', 'Task2', 'Task3']
        self.project_file_handle.project_create(project_name)
        self.project_file_handle.task_create(project_name, 'web', task_name)
        delete_results = self.project_file_handle.task_delete(project_name, task_name)
        self.project_file_handle.project_delete(project_name)
        assert len(delete_results) == 3
        for name in task_name:
            assert name in delete_results.keys()
            assert delete_results[name]


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_07_01_get_tasks(self):
        """ 获取项目的任务列表 """
        project_name = 'Project1'
        task_name = ['Task1', 'Task2', 'Task3']
        self.project_file_handle.project_create(project_name)
        self.project_file_handle.task_create(project_name, 'web', task_name)
        task_list = self.project_file_handle.get_tasks(project_name)
        self.project_file_handle.project_delete(project_name)
        assert len(task_list) == 3
        for name in task_name:
            assert name in task_list


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_07_02_get_tasks(self):
        """ 获取项目的任务列表 """
        project_name = 'Project1'
        bas_task_name = ['Task1', 'Task2', 'Task3']
        new_task_name = ['NTask1', 'NTask2', 'NTask3']
        self.project_file_handle.project_create(project_name)
        self.project_file_handle.task_create(project_name, 'web', bas_task_name)
        rename_results = self.project_file_handle.task_rename(project_name, bas_task_name, new_task_name) 
        task_list = self.project_file_handle.get_tasks(project_name)
        self.project_file_handle.project_delete(project_name)
        assert len(task_list) == 3
        for old_name, new_name in list(zip(bas_task_name, new_task_name)):
            assert old_name in rename_results.keys()
            assert rename_results[old_name]
            assert new_name in task_list
