"""
[ unit testing :: awaken2.baseic.common.configure.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))


from pathlib import Path

from src.awaken2.baseic.const import G_CONST
from src.awaken2.baseic.common.configure import Configure
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception


class TestAwakenLog(object):
    config_path: Path

    def setup_class(self):
        self.config_path = G_CONST.Path.ACTUALLY_CWD.joinpath('test_config.yaml')


    def setup(self):
        self.config_path.touch()


    def teardown(self):
        self.config_path.unlink()


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_init(self):
        config = Configure(self.config_path)
        assert len(config.keys()) == 0


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_02_init(self):
        config = Configure(self.config_path, {'tk1': 'tv1', 'tk2': 'tv2'})
        assert len(config.keys()) == 2


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_01_get(self):
        config = Configure(self.config_path, {'tk1': 'tv1', 'tk2': 'tv2'})
        tk1 = config.get('tk1')
        assert tk1 == 'tv1'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_02_get(self):
        config = Configure(self.config_path, {'tk1': 'tv1', 'tk2': 'tv2'})
        tk1 = config.get('tk3')
        assert tk1 == None


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_01_set(self):
        config = Configure(self.config_path)
        old_tk1 = config.get('tk1')
        config.set('tk1', 'tv1')
        new_tk1 = config.get('tk1')
        assert old_tk1 == None
        assert new_tk1 == 'tv1'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_01_set(self):
        config = Configure(self.config_path)
        config.set('tk1', 'tv1')
        config.set('tk1', 'tv2')
        tk1 = config.get('tk1')
        assert tk1 == 'tv2'


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_01_delete(self):
        config = Configure(self.config_path)
        config.set('tk1', 'tv1')
        config.delete('tk1')
        keys = config.keys()
        tk1 = config.get('tk1')
        assert len(keys) == 0
        assert tk1 == None


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_01_delete(self):
        config = Configure(self.config_path, {'tk1': 'tv1', 'tk2': 'tv2'}, ['tk1'])
        config.delete('tk1')
        tk1 = config.get('tk1')
        assert tk1 == 'tv1'
