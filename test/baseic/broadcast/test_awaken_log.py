"""
[ unit testing :: awaken2.baseic.broadcast.awaken_log.py ]

"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))


from src.awaken2.baseic.broadcast.awaken_log import AwakenLog
from src.awaken2.baseic.decorator.unit_testing import deco_test_capture_exception


class TestAwakenLog(object):
    awaken_log: AwakenLog


    def setup_class(self):
        self.awaken_log = AwakenLog()


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_01_01_debug(self):
        self.awaken_log.debug('debug')


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_02_01_info(self):
        self.awaken_log.info('info')


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_03_01_error(self):
        self.awaken_log.error('error')


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_04_01_warning(self):
        self.awaken_log.warning('warning')


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_05_01_critical(self):
        self.awaken_log.critical('critical')


    # ----------------------------------------------------------------------
    @deco_test_capture_exception
    def test_06_01_out_map(self):
        self.awaken_log.out_map('debug', 'debug')
