import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


import shutil
import logging

from src.awaken2.baseic.const import G_CONST
from src.awaken2.baseic.environment.common import environment_check_browser_driver_exists
from src.awaken2.baseic.environment.common import environment_check_dependent_files_exists


# 创建框架运行环境
environment_check_browser_driver_exists()
environment_check_dependent_files_exists()


try:
    # 执行测试命令
    os.system('pytest ./server')
except BaseException:
    ...

finally:
    # 注销
    logging.shutdown()
    # 删除单元测试文件
    shutil.rmtree(G_CONST.Path.DirPath.Data)
    G_CONST.Path.FilePath.EngineeringInit.unlink()
