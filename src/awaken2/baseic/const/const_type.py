# Copyright 2022 quinn.7@foxmail.com All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
类型常量

"""


# --------------------------------------------------------------------------
class Task:
    """
    [ 任务类型常量 ]

    """
    Web = 'WEB'
    """ WEB """

    Api = 'API'
    """ API """


# --------------------------------------------------------------------------
class Browser:
    """
    [ 浏览器类型常量 ]

    """
    Webkit   = 'webkit'
    """ WebKit浏览器 """

    Firefox  = 'firefox'
    """ 火狐浏览器 """

    Chromium = 'chromium'
    """ 谷歌浏览器 """
